import scrapy
from scrapy.http import FormRequest

class LoginSpider(scrapy.Spider):
    name = 'login'
    allowed_domains = ['codeforces.com']
    start_urls = ['https://codeforces.com/enter']


    def parse(self, response):

        csrf_token = response.xpath('//*[@class="csrf-token"]/@data-csrf').extract()
        ftaa = ''
        bfaa = ''
        _tta = '618'
        
        yield FormRequest('https://codeforces.com/enter',
                formdata={'csrf_token': csrf_token,
                          'action': 'enter',
                          'ftaa': ftaa,
                          'bfaa': bfaa,
                          'handleOrEmail': 'USERNAME',  # Replace with your username
                          'password': 'PASSWORD',       # Replace with your password
                          '_tta': _tta},
                callback=self.parse_after_login)

    def parse_after_login(self, response):
        if response.xpath('//a[text()="Logout"]'):
             yield scrapy.Request('https://codeforces.com/problemset/submit',
                                    callback=self.submit_code)

    def submit_code(self, response):
        # now at the submission page send form request to submit the code

        csrf_token = response.xpath('//*[@class="csrf-token"]/@data-csrf').extract()
        ftaa = ''
        bfaa = ''
        lang = '54' # 54 for c++
        _tta = '618'

        f = open('data.txt')
        problem = f.readline()
        code = f.read()
        f.close()

        if response.xpath('//*[@class="submit"]'):
            yield FormRequest('https://codeforces.com/problemset/submit',
                formdata={'csrf_token': csrf_token,
                          'ftaa': ftaa,
                          'bfaa': bfaa,
                          'action': 'submitSolutionFormSubmitted',
                          'submittedProblemCode': problem,
                          'programTypeId': lang,
                          'source': code,
                          'tabSize': '4',
                          'sourceFile': '',
                          '_tta': _tta},
                callback=self.check_solution)


    def check_solution(self, response):
        pass # here a method can be added for checking if the solution was accepted


