#!/bin/bash

# Replace <PATH> with directory path to file

x=$(perl current.pl)
cat $x | grep -i -m 1 Problem | sed -r 's/\/\/\s+Problem\s+//' | sed -r 's/\s+[^>]*//' > <PATH>cf_login_spider/cf_login_spider/spiders/data.txt
cat $x >> <PATH>/cf_login_spider/cf_login_spider/spiders/data.txt
cd <PATH>/cf_login_spider/cf_login_spider/spiders
scrapy crawl login --nolog
rm <PATH>/cf_login_spider/cf_login_spider/spiders/data.txt
cd <PATH>/codeforces
