# Codeforces Autosubmit

This program automatically submits code for problems on codeforces

## Installation
- Scrapy package for python must be installed
- edit login.py (replace USERNAME and PASSWORD)
```
cd <PATH TO YOUR SCRAPY DIRECTORY>
scrapy genspider cf_login_spider
mv <PATH TO FILE>/login.py cf_login_spider/cf_login_spider/spiders
```
- edit submit.sh (add paths where necessary)
